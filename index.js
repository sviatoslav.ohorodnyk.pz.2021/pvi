document.getElementById("add-student").addEventListener("click", function () {
    var modal = document.getElementById("student-data-modal");
    modal.style.display = "block";
    document.getElementById("collect-data").value = "Create";
    document.getElementById("student-data-header").innerHTML = "Add student";

    //deleting all event listeners
    var el = document.getElementById("collect-data"),
        elClone = el.cloneNode(true);
    el.parentNode.replaceChild(elClone, el);

    document.getElementById("collect-data").addEventListener("click", addNewStudentData);
    clearData(document.querySelectorAll(".student-data"));
});

document.getElementById("close-student-data-modal").addEventListener("click", function () {
    closeModal(document.getElementById("student-data-modal"));
});
/*
document.getElementById("close-delete-student-modal").addEventListener("click", function () {
    closeModal(document.getElementById("delete-student-modal"))
});*/

let closeButtons = document.querySelectorAll("#delete-student-modal .close").forEach(function (item) {
    item.addEventListener("click", function () {
        closeModal(document.getElementById("delete-student-modal"));
    });
});
let rowToDelete;
document.getElementById("delete-row").addEventListener("click", function () {
    rowToDelete.remove();
    closeModal(document.getElementById("delete-student-modal"));
});
function deleteRow(row) {
    var modal = document.getElementById("delete-student-modal");
    modal.style.display = "block";

    var studentName = row.cells[2].innerHTML;
    document.getElementById("delete-student-warning").innerHTML = "Are you sure you want to delete user " + studentName + "?";
    rowToDelete = row;
}
function changeStatus(checkBox) {
    var statusImg = checkBox.parentNode.parentNode.getElementsByTagName("img")[0];
    const parent = statusImg.parentNode;
    if (checkBox.checked) {
        const newNode = document.createElement("img");
        newNode.src = "Images\\Green Circle.png";
        newNode.alt = "user-online";
        newNode.classList.add("user-online-img");
        parent.replaceChild(newNode, statusImg);
    }
    else {
        const newNode = document.createElement("img");
        newNode.src = "Images\\Gray Circle.png";
        newNode.alt = "user-offline";
        newNode.classList.add("user-online-img");
        parent.replaceChild(newNode, statusImg);
    }
}

function clearData(studentsData) {
    for (var i = 0; i < studentsData.length; i++) {
        if (studentsData[i].tagName == "SELECT") {
            studentsData[i].selectedIndex = 0;
        }
        else {
            studentsData[i].value = "";
        }
    }
}

function addNewStudentData() {
    var data = collectData();

    if (data.length != 0) {
        fillRow(data);
        closeModal(document.getElementById("student-data-modal"));
    }
};

function collectData() {
    var data = [];
    var studentsData = document.querySelectorAll(".student-data");
    for (var i = 0; i < studentsData.length; i++) {
        if (studentsData[i].value.length == 0) {
            alert("Введіть усі дані!");
            return [];
        }
        data.push(studentsData[i].value);
    }
    return data;
}

function editStudentData(row) {
    var data = collectData();

    if (data.length != 0) {
        editRow(row.cells, data);
    }
}

function fillRow(data) {
    var table = document.getElementById("students-table");
    var tBody = table.getElementsByTagName("tbody")[0];
    var row = tBody.insertRow();

    var cells = [];
    const AMOUNT_OF_CELLS = table.rows[0].cells.length;
    for (var i = 0; i < AMOUNT_OF_CELLS; i++) {
        cells.push(row.insertCell(i));
        switch (i) {
            case 0:
                cells[i].innerHTML = "<input type='checkbox' onchange = 'changeStatus(this)'>";
                break;
            case 1:
                cells[i].innerHTML = "PZ-" + data[0];
                break;
            case 2:
                cells[i].innerHTML = data[1] + " " + data[2];
                break;
            case 3:
                cells[i].innerHTML = data[3];
                break;
            case 4:
                cells[i].innerHTML = data[4];
                break;
            case 5:
                cells[i].innerHTML = "<img src = 'Images/Gray Circle.png' alt = 'user_online_img' class = 'user-online-img'>";
                break;
            case 6:
                cells[i].innerHTML = "<input type='button' value=' ' class = 'edit-student'> <input type='button' value='X' onclick='deleteRow(this.parentNode.parentNode)'>";

                cells[i].firstElementChild.addEventListener("click", function () {
                    var modal = document.getElementById("student-data-modal");
                    modal.style.display = "block";
                    document.getElementById("collect-data").value = "Save";
                    document.getElementById("student-data-header").innerHTML = "Edit student";

                    var el = document.getElementById("collect-data"),
                        elClone = el.cloneNode(true);

                    el.parentNode.replaceChild(elClone, el);

                    document.getElementById("collect-data").addEventListener("click", function () {
                        editStudentData(row)
                    });

                    var studentsData = document.querySelectorAll(".student-data");
                    studentsData[0].selectedIndex = cells[1].innerHTML.slice(-1) - 1;
                    studentsData[1].value = cells[2].innerHTML.split(' ')[0];
                    studentsData[2].value = cells[2].innerHTML.split(' ')[1];
                    if (cells[3].innerHTML == "M") {
                        studentsData[3].selectedIndex = 0;
                    }
                    else studentsData[3].selectedIndex = 1;
                    studentsData[4].value = cells[4].innerHTML;
                });
                break;
        }
    }
}

function editRow(cells, data) {
    cells[1].innerHTML = "PZ-" + data[0];
    cells[2].innerHTML = data[1] + " " + data[2];
    cells[3].innerHTML = data[3];
    cells[4].innerHTML = data[4];

    closeModal(document.getElementById("student-data-modal"))
}
function closeModal(modal) {
    modal.style.display = "none";
}